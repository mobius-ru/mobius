<?php

namespace app\controllers;

use app\helpers\JsonResponse;
use app\models\AcceptanceRequestGoods;
use app\models\Contract;
use app\models\Document;
use app\models\Goods;
use app\services\_1C_SoapEndpoint;
use app\services\SoapDto;
use Yii;
use app\models\AcceptanceRequest;
use app\models\AcceptanceRequestSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AcceptanceRequestController implements the CRUD actions for AcceptanceRequest model.
 */
class AcceptanceRequestController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all AcceptanceRequest models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AcceptanceRequestSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single AcceptanceRequest model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AcceptanceRequest model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AcceptanceRequest();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionSave(){
        $model = new AcceptanceRequest();
        $model->setAttributes(Yii::$app->request->post());
        $transaction = Yii::$app->db->beginTransaction();
        if(!$model->save()) {
            $transaction->rollBack();
            JsonResponse::error(500, $model->errors);
        }
        foreach (Yii::$app->request->post('products') as $product) {
            if(!$product['id']) {
                $newProduct = new Goods();
                $newProduct->setAttributes($product);
                $newProduct->user_id = $model->contract->user_id;
                $newProduct->name = 'test'; //TODO

                if(!$newProduct->save()) {
                    $transaction->rollBack();
                    JsonResponse::error(500, $newProduct->errors);
                }
                $product['id'] = $newProduct->id;
            }
            $link = new AcceptanceRequestGoods();
            $link->acceptance_request_id = $model->id;
            $link->goods_id = $product['id'];
            $link->count = $product['quantity'];
            if(!$link->save()) {
                $transaction->rollBack();
                JsonResponse::error(500, $link->errors);
            }
        }
        $api = new _1C_SoapEndpoint();
        $document = new Document();
        if($documentDto = $api->sendAcceptanceRequest()) {
            SoapDto::mapFrom1C($documentDto, $document);
            $document->contract_id = $model->contract_id;
            if(!$document->save()) {
                $transaction->rollBack();
                JsonResponse::error(500, $document->errors);
            }
        } else {
            $transaction->rollBack();
            JsonResponse::error(500, 'soap error');
        }
        $model->contract->status = Contract::STATUS_PROCESSING;
        if(!$model->contract->save()) {
            $transaction->rollBack();
            JsonResponse::error(500, $model->contract->errors);
        }
        $transaction->commit();
        JsonResponse::success($document->attributes);
    }

    /**
     * Updates an existing AcceptanceRequest model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing AcceptanceRequest model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AcceptanceRequest model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AcceptanceRequest the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AcceptanceRequest::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
