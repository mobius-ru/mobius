<?php

namespace app\controllers;

use app\models\Company;
use app\models\Contract;
use app\services\_1C_SoapEndpoint;
use app\services\SoapDto;
use Yii;
use app\models\User;
use app\models\UserSearch;
use yii\base\Exception;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws \yii\db\Exception
     */
    public function actionCreate()
    {
        $model = new User();

        if ($model->load(Yii::$app->request->post())) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                if($model->role_id == User::ROLE_CLIENT) {
                    $api = new _1C_SoapEndpoint();
                    if ($model->role_id == User::ROLE_CLIENT && $userInfo = $api->getUserInfoByContractNumber($model->contract_number)) {
                        SoapDto::mapFrom1C($userInfo->user, $model);
                        if(!$model->save()) {
                            $transaction->rollBack();
                            throw new Exception(var_export($model->errors, true));
                        }
                        $newCompany = new Company();
                        $newCompany->user_id = $model->id;
                        SoapDto::mapFrom1C($userInfo->company, $newCompany);
                        if(!$newCompany->save()) {
                            $transaction->rollBack();
                            throw new Exception(var_export($newCompany->errors, true));
                        } else {
                            foreach ($userInfo->contracts as $contract) {
                                $newContract = new Contract();
                                $newContract->user_id = $model->id;
                                SoapDto::mapFrom1C($contract, $newContract);
                                if(!$newContract->save()) {
                                    $transaction->rollBack();
                                    throw new Exception(var_export($newContract->errors, true));
                                    break;
                                }
                            }
                        }
                    }
                } else {
                    $model->save();
                }
            } catch (\Exception $e) {
                $transaction->rollBack();
                $model->addError('contract_number',$e->getMessage());
            }
            if($transaction->isActive && $model->sendMail("Пароль: $model->password",'Регистрация нового пользователя')){
                $transaction->commit();
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                $model->addError('password_hash',"Не удалось отправить сгенерированный пароль пользователю");
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
