<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ContractSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Договоры';
$this->params['breadcrumbs'][] = $this->title;

    $columns = [];
    if(Yii::$app->user->identity->role_id !== \app\models\User::ROLE_CLIENT) {
        array_push($columns, ['attribute' => 'company_name','label' => 'Компания', 'value'=>'user.company.name']);
        array_push($columns, ['attribute' => 'company_inn','label' => 'ИНН', 'value'=>'user.company.inn']);
        array_push($columns, ['attribute' => 'user_name','label' => 'ФИО', 'value'=>'user.fio']);
    }
    array_push($columns, 'number');
    array_push($columns, 'date_of_conclusion');
    array_push($columns, [
        'attribute' => 'status',
        'value' => function ($model, $key, $index, $widget) { return \app\models\Contract::$statuses[$model->status]; },
        'filter' => \app\models\Contract::$statuses
    ]);
    array_push($columns, ['class' => 'yii\grid\ActionColumn', 'template' => '{view}']);
?>
<div class="contract-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать договор', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $columns,
    ]); ?>

    <?php Pjax::end(); ?>

</div>
