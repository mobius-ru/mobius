<?php

use kartik\grid\GridView;
use yii\bootstrap\Tabs;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Contract */
/* @var $acceptanceRequestModel app\models\AcceptanceRequest */
/* @var $newDocumentModel app\models\Document */
/* @var $goodsModels app\models\Goods */

$this->title = "Договор $model->number";
$this->params['breadcrumbs'][] = ['label' => 'Договоры', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->number;
\yii\web\YiiAsset::register($this);
?>
<div class="contract-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'id',
            'number',
            'date_of_conclusion',
            'user.company.name',
            'user.fio',
            ['attribute' => 'status', 'value' => \app\models\Contract::$statuses[$model->status]],

        ],
    ]) ?>

    <?php

    $products = [];
    foreach ($goodsModels = \app\models\Goods::findAll(['user_id' => $model->user_id]) as $product){
        array_push($products, $product->attributes);
    }

    $documents = [];
    foreach ($model->documents as $documentModel) {
        array_push($documents, $documentModel->attributes);
    }

    $acceptanceProducts = [];
    if($model->acceptanceRequest) {
        foreach ($model->acceptanceRequest->acceptanceRequestGoods as $acceptanceLink) {
            $product = new stdClass();
            $product->id = $acceptanceLink->goods_id;
            $product->name = $acceptanceLink->goods->name;
            $product->unit = $acceptanceLink->goods->unit;
            $product->price = $acceptanceLink->goods->price;
            $product->quantity = $acceptanceLink->count;
            $product->status = $acceptanceLink->status;
            $product->balance = $acceptanceLink->goods->balance;
            array_push($acceptanceProducts, $product);
        }
    }

    $shipmentProducts = [];
    if($model->shipmentRequest) {
        foreach ($model->shipmentRequest->shipmentRequestGoods as $shipmentLink) {
            $product = new stdClass();
            $product->id = $shipmentLink->goods_id;
            $product->name = $shipmentLink->goods->name;
            $product->unit = $shipmentLink->goods->unit;
            $product->price = $shipmentLink->goods->price;
            $product->quantity = $shipmentLink->count;
            $product->status = $shipmentLink->status;
            $product->balance = $shipmentLink->goods->balance;
            array_push($shipmentProducts, $product);
        }
    }

    ?>

    <link rel="stylesheet" type="text/css" href="/css/angular.css">

    <script src="/js/angular/angular.min.js"></script>
    <script src="/js/angular/angular-datepicker.js"></script>
    <script src="/js/angular/directives.js"></script>

    <script>
        var app = angular.module('app',['directives', '720kb.datepicker']);
        app.controller('AppController', ['$scope', '$timeout', function($scope, $timeout){
            $scope.data = {
                user: {
                    role: 1,// 1-админ 2 манагер 3 клиент
                    id: '<?= Yii::$app->user->id ?>',
                },
                contractId: '<?= $model->id ?>',
                contractOwnerId: '<?= $model->user_id ?>',
                forms: {
                    acceptance: {
                        date: '<?= $model->acceptanceRequest ? date('d.m.Y',strtotime($model->acceptanceRequest->date)) : '' ?>',
                        address: '<?= $model->acceptanceRequest ? $model->acceptanceRequest->address : ''?>',
                        comment: '<?= $model->acceptanceRequest ? $model->acceptanceRequest->comment_message : ''?>',
                        additionalServices: '<?= $model->acceptanceRequest ? $model->acceptanceRequest->additional_services : ''?>',
                        responsible: '<?= $model->acceptanceRequest ? $model->acceptanceRequest->responsible : ''?>',
                        products: <?= json_encode($acceptanceProducts) ?>
                    },
                    shipment: {
                        date: '<?= $model->shipmentRequest ? date('d.m.Y',strtotime($model->shipmentRequest->date)) : '' ?>',
                        address: '<?= $model->shipmentRequest ? $model->shipmentRequest->address: '' ?>',
                        comment: '<?= $model->shipmentRequest ? $model->shipmentRequest->comment_message : '' ?>',
                        additionalServices: '<?= $model->shipmentRequest ? $model->shipmentRequest->additional_services : '' ?>',
                        responsible: '<?= $model->shipmentRequest ? $model->shipmentRequest->responsible : '' ?>',
                        products: <?= json_encode($shipmentProducts) ?>
                    },
                    doc: {
                        chooseId: '',
                        docType: null,
                        type: '',
                        date: '',
                        attachment: null,
                        list: <?= json_encode($documents) ?>,
                    },
                    product: {
                        chooseId: '',
                        product: null,
                        id: '',
                        name: '',
                        unit: '',
                        price: '',
                        quantity: 0,
                        balance: 0,
                    }
                },
                validate: {
                    show: false,
                    forms: {
                        acceptance:{
                            date:       {required: true, error: ''},
                            address:    {required: true},
                            products:   {if: function(value){return value.length ? '' : 'Необходимо добавить хотя бы один продукт'}},
                        },
                        shipment:{
                            date:       {required: true},
                            address:    {required: true},
                            products:   {if: function(value){return value.length ? '' : 'Необходимо добавить хотя бы один продукт'}},
                        },
                        doc:{
                            date:       {required: true},
                            type:       {required: true},
                            attachment: {required: true},
                        },
                        product:{
                            id:         {if: function(value){return !value && $scope.productWindow.tabs.choose === 'exist'  ? 'Необходимо выбрать продукт' : ''}},
                            name:       {if: function(value){return !value && $scope.productWindow.tabs.choose === 'new'  ? 'Поле обязательно для заполнения' : ''}},
                            unit:       {if: function(value){return !value && $scope.productWindow.tabs.choose === 'new'  ? 'Поле обязательно для заполнения' : ''}},
                            price:      {if: function(value){return !value && $scope.productWindow.tabs.choose === 'new'  ? 'Поле обязательно для заполнения' : ''}},
                            quantity:   {required: true},
                            balance:    {if: function(value){return !value && $scope.productWindow.tabs.choose === 'new'  ? 'Поле обязательно для заполнения' : ''}},
                        },
                    }
                },
                lists: {
                    products: <?= json_encode($products) ?>,
                },
                objLists: {
                    products: {},
                    docTypes: <?= json_encode(\app\models\Document::$types) ?>,
                    docStatuses: <?= json_encode(\app\models\Document::$statuses) ?>,
                    productStatuses: <?= json_encode(\app\models\AcceptanceRequestGoods::$statuses) ?>,
                }
            };

            $scope.productWindow = {
                isOpen: false,
                tabs: {
                    tabs: {
                        exist:  {title: 'Добавить существующий товар'},
                        new:    {title: 'Добавить новый товар'},
                    },
                    choose: 'exist',
                    disabled: false,
                    change: function(key){
                        $scope.clearProductFields($scope.data.forms.product);
                    }
                },
                add: function () {
                    $scope.addProduct(function(){
                        $scope.productWindow.isOpen = false;
                    });
                },
                close: function () {
                    $scope.productWindow.isOpen = false;
                    $scope.clearProductFields($scope.data.forms.product);
                },
                open: function () {
                    $scope.productWindow.isOpen = true;
                }
            };
            $scope.dataTabs = {
                tabs:{
                    acceptance:{title: 'Заявка на приемку (поступление товара на склад)'},
                    shipment:{title: 'Заявка на отгрузку (выдача товара со склада)'},
                    doc:{title: 'Документы'},
                },
                choose: 'acceptance',
                disabled: false,
                change: function(key){

                }
            };
            $scope.loader = {// показывается ли лоадер (поверх декларации)
                show: false,
                text: '',
                open: function(text){
                    $scope.loader.text = text || '';
                    $scope.loader.show = true;
                },
                close: function(){
                    $scope.loader.text = '';
                    $scope.loader.show = false;
                }
            };
            $scope.createObjectFromList = function(arr){
                var obj = {};
                $.each(arr, function(i, item){
                    obj[item.id] = item;
                });
                return obj;
            };
            $scope.createListFromObject = function(obj){
                var arr = [];
                $.each(obj, function(key, value){
                    arr.push({key: key, value: value});
                });
                return arr;
            };

            $scope.data.objLists.products = $scope.createObjectFromList($scope.data.lists.products);
            $scope.data.lists.docTypes = $scope.createListFromObject($scope.data.objLists.docTypes);

            $scope.getFile = function(e, scope, type){console.log('e.target.files', e.target.files);
                $scope.data.forms.doc.attachment = e.target.files[0];
                $scope.$apply();
            };

            $scope.removeProduct = function(place, index){
                place.splice(index, 1);
            };
            $scope.getErrorField = function(value, params){
                let isNumber = typeof value === 'number',
                    error = '';

                if (params.required && !value) {
                    error = 'Поле обязательно для заполнения';
                } else if (params.min && ((isNumber && value < params.min) || (!isNumber && value.length < params.min))) {
                    error = 'Минимальное значение - ' + params.min;
                } else if (params.max && ((isNumber && value > params.max) || (!isNumber && value.length > params.max))) {
                    error = 'Максимальное значение - ' + params.min;
                } else if (params.format && !value.match(params.format)) {
                    error = 'Значение не соответствует формату';
                } else if (params.if) {
                    error = params.if(value) || '';
                }
                return error;
            };
            $scope.validate = function(keyForm){
                var errors = '',
                    rules = $scope.data.validate.forms[keyForm];
                $.each(rules, function(key, params){
                    params.error = $scope.getErrorField($scope.data.forms[keyForm][key], params);
                    errors += params.error;console.log('errors',key, params.error)
                });

                $scope.data.validate.show = Boolean(errors);

                return !errors;
            };


            $scope.addNewProduct = function(product, func){
                $scope.loader.open();
                $.ajax({
                    type: 'POST',
                    data: {
                        name: product.name,
                        unit: product.unit,
                        balance: product.balance,
                        price: product.price,
                        user_id: $scope.data.contractOwnerId
                    },
                    url: '<?= \yii\helpers\Url::to(['//goods/add']) ?>',
                    success: function (data) {
                        $scope.loader.close();
                        data = JSON.parse(data);
                        func(data.result);
                        $scope.$apply();
                    },
                    error: function (error) {
                        $scope.loader.close();
                        console.log(error);
                        alert('Ошибка');
                        $scope.$apply();
                    }
                });
            };
            $scope.addProduct = function(func){
                if($scope.validate('product')){
                    var product = $scope.data.forms.product;

                    if($scope.productWindow.tabs.choose === 'new'){
                        $scope.addNewProduct(product, function(id){
                            product.id = id;
                            $scope.data.lists.products.push({
                                id: product.id,
                                unit: product.unit,
                                name: product.name,
                                balance: product.balance
                            });
                            $scope.data.objLists.products = $scope.createObjectFromList($scope.data.lists.products);
                            $scope.addProductInForm(product);
                            func();
                        });
                    }else{
                        $scope.addProductInForm(product);
                        func();
                    }
                }
            };
            $scope.addProductInForm = function(product){
                $scope.data.forms[$scope.dataTabs.choose].products.push({
                    id: product.id,
                    unit: product.unit,
                    price: product.price,
                    quantity: product.quantity,
                    status: 1
                });
                $scope.clearProductFields(product);
            };
            $scope.clearProductFields = function(product){
                product.id = '';
                product.name = '';
                product.unit = '';
                product.price = '';
                product.balance = '';
                product.quantity = '';
                product.chooseId = '';
                product.product = null;
            };

            $scope.saveAcceptance = function(e){
                e.preventDefault();
                if($scope.validate('acceptance')) {
                    $scope.loader.open();
                    var form = $scope.data.forms.acceptance;
                    $.ajax({
                        type: 'POST',
                        data: {
                            date: form.date,
                            address: form.address,
                            comment_message: form.comment,
                            additional_services: form.additionalServices,
                            responsible: form.responsible,
                            products: form.products,
                            contract_id: $scope.data.contractId
                        },
                        url: '<?= \yii\helpers\Url::to(['//acceptance-request/save']) ?>',
                        success: function (data) {
                            $scope.loader.close();
                            var doc = JSON.parse(data).result;
                            $scope.addDocument(doc.id, doc.type, doc.date, doc.status, doc.name);

                            $scope.$apply();
                        },
                        error: function () {
                            $scope.loader.close();
                            alert('Ошибка');
                            $scope.$apply();
                        }
                    });
                }
            };
            $scope.saveShipment = function(e){
                e.preventDefault();
                if($scope.validate('shipment')) {
                    $scope.loader.open();
                    var form = $scope.data.forms.shipment;
                    $.ajax({
                        type: 'POST',
                        data: {
                            date: form.date,
                            address: form.address,
                            comment_message: form.comment,
                            additional_services: form.additionalServices,
                            responsible: form.responsible,
                            products: form.products,
                            contract_id: $scope.data.contractId
                        },
                        url: '<?= \yii\helpers\Url::to(['//shipment-request/save']) ?>',
                        success: function (data) {
                            $scope.loader.close();
                            var doc = JSON.parse(data).result;
                            $scope.addDocument(doc.id, doc.type, doc.date, doc.status, doc.name);

                            $scope.$apply();
                        },
                        error: function () {
                            $scope.loader.close();
                            alert('Ошибка');
                            $scope.$apply();
                        }
                    });
                }
            };
            $scope.saveDocuments = function(e){
                e.preventDefault();

                if($scope.validate('doc')) {
                    $scope.loader.open();
                    var form = $scope.data.forms.doc;
                    var filesData = new FormData();
                    filesData.append('date', form.date);
                    filesData.append('type', form.type);
                    filesData.append('file', form.attachment, form.attachment.name);
                    filesData.append('contract_id', $scope.data.contractId);

                    $.ajax({
                        type: 'POST',
                        processData: false, // important
                        contentType: false, // important
                        data: filesData,
                        url: '<?= \yii\helpers\Url::to(['//documents/create']) ?>',
                        success: function (data) {
                            $scope.loader.close();
                            var id = JSON.parse(data).result;
                            $scope.addDocument(id, form.type, form.date, 1, form.attachment.name);
                            $scope.clearDocumentFields(form);
                            $scope.$apply();
                        },
                        error: function () {
                            $scope.loader.close();
                            alert('Ошибка');
                            $scope.$apply();
                        }
                    });
                }
            };
            $scope.addDocument = function(id, type, date, status, name){
                $scope.data.forms.doc.list.push({
                    id: id,
                    type: type,
                    date: date,
                    status: status,
                    name: name,
                });
            };

            $scope.clearDocumentFields = function(form){
                form.type = '';
                form.date = '';
                form.attachment = null;
                form.chooseId = '';
                form.docType = null;
            };

        }]);
    </script>

    <div ng-app="app">
        <div id="contract-data-block" ng-controller="AppController">
            <div class="loader_overlay" ng-if="loader.show">
                <div class="loader_spinner"></div>
                <div class="margin_T20 loader_text"><!--{{loader.text}}--></div>
            </div>
            <div>
                <div modal-window-light
                     open="productWindow.isOpen"
                     close="productWindow.close"
                     window-title="'Продукт'"
                     window-style="{width: '600px', height: '550px'}"
                     buttons="[{title: 'Добавить', classes: 'btn btn-default pull-right', click: productWindow.add}]"
                >
                    <div wizard-tab="productWindow.tabs"></div>
                    <div class="tab_container">
                        <div ng-if="productWindow.tabs.choose === 'exist'">
                            <label for="">Выберите товар из списка*</label>
                            <div
                                special-select
                                ng-model="data.forms.product.id"
                                choose-option-id="data.forms.product.chooseId"
                                select-option="data.lists.products"
                                choose-option="data.forms.product.product"
                                instead-key="id"
                                instead-value="['name']"
                                empty-row="Выбрать товар"
                                empty-value-text="Товар"
                            ></div>
                            <div validation-error="data.validate.forms.product.id" ng-model="data.forms.product.id" show-error="data.validate.show"></div>
                        </div>
                        <div ng-if="productWindow.tabs.choose === 'new'">
                            <div class="p">
                                <label for="">Введите наименование товара*</label>
                                <input class="input" type="text" ng-model="data.forms.product.name">
                                <div validation-error="data.validate.forms.product.name" ng-model="data.forms.product.name" show-error="data.validate.show"></div>
                            </div>
                            <div class="p">
                                <label for="">Остаток*</label>
                                <input class="input" type="text" ng-model="data.forms.product.balance">
                                <div validation-error="data.validate.forms.product.balance" ng-model="data.forms.product.balance" show-error="data.validate.show"></div>
                            </div>
                            <div class="p">
                                <label for="">Ед.измерения</label>
                                <input class="input" type="text" ng-model="data.forms.product.unit">
                                <div validation-error="data.validate.forms.product.unit" ng-model="data.forms.product.unit" show-error="data.validate.show"></div>
                            </div>
                            <div class="p">
                                <label for="">Цена</label>
                                <input class="input" type="text" ng-model="data.forms.product.price">
                                <div validation-error="data.validate.forms.product.price" ng-model="data.forms.product.price" show-error="data.validate.show"></div>
                            </div>
                        </div>
                        <div class="p">
                            <label for="">Кол-во*</label>
                            <input class="input" type="text" ng-model="data.forms.product.quantity">
                            <div validation-error="data.validate.forms.product.quantity" ng-model="data.forms.product.quantity" show-error="data.validate.show"></div>
                        </div>
                    </div>
                </div>
                <div>
                    <div wizard-tab="dataTabs"></div>
                    <div class="tab_container">
                        <div style="padding: 15px 15px 0 15px;">
                            <!--------------------------------------------------------------------------------------------------------------->
                            <div ng-if="dataTabs.choose === 'acceptance'">
                                <form ng-submit="saveAcceptance($event)">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="">Дата приема товара</label>
                                            <datepicker date-format="dd.MM.yyyy">
                                                <input class="input" type="text" ng-model="data.forms.acceptance.date">
                                            </datepicker>
                                            <div validation-error="data.validate.forms.acceptance.date" ng-model="data.forms.acceptance.date" show-error="data.validate.show"></div>
                                        </div>
                                        <div class="col-md-4">
                                            <label for="">Адрес склада</label>
                                            <input class="input" type="text" ng-model="data.forms.acceptance.address">
                                            <div validation-error="data.validate.forms.acceptance.address" ng-model="data.forms.acceptance.address" show-error="data.validate.show"></div>
                                        </div>
                                        <div class="col-md-4">
                                            <label for="">Ответственный</label>
                                            <input class="input" type="text" ng-model="data.forms.acceptance.responsible">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label for="">Комментарий</label>
                                            <textarea class="input textarea" ng-model="data.forms.acceptance.comment"></textarea>
                                        </div>
                                        <div class="col-md-12">
                                            <label for="">Дополнительные складские услуги</label>
                                            <textarea class="input textarea" ng-model="data.forms.acceptance.additionalServices"></textarea>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table class="table table-striped table-bordered detail-view" style="margin-top: 20px;">
                                                <colgroup>
                                                    <col width="">
                                                    <col width="120px">
                                                    <col width="180px">
                                                    <col width="120px">
                                                    <col width="120px">
                                                    <col width="30px">
                                                </colgroup>
                                                <thead class="thead-default">
                                                <tr>
                                                    <th>Товар</th>
                                                    <th>Ед.изм.</th>
                                                    <th>Цена</th>
                                                    <th>Кол-во*</th>
                                                    <th>Статус</th>
                                                    <th></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr ng-repeat="(productID, product) in data.forms.acceptance.products track by $index">
                                                    <td>{{data.objLists.products[product.id].name}}</td>
                                                    <td>{{product.unit || data.objLists.products[product.id].unit}}</td>
                                                    <td>{{product.price || data.objLists.products[product.id].price}}</td>
                                                    <td>{{product.quantity}}</td>
                                                    <td>{{data.objLists.productStatuses[product.status]}}</td>
                                                    <td><a ng-click="removeProduct(data.forms.acceptance.products, productID)"><i class="glyphicon glyphicon-remove"></i></a></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <div validation-error="data.validate.forms.acceptance.products" ng-model="data.forms.acceptance.products" show-error="data.validate.show"></div>
                                            <a class="btn btn-default pull-right" ng-click="productWindow.open()">Добавить товар</a>

                                        </div>
                                    </div>

                                    <div class="row" style="margin-top: 20px;">
                                        <div class="col-md-12">
                                            <button type="submit" class="btn btn-primary pull-right" ng-click="saveAcceptance($event)">Отправить заявку</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!--------------------------------------------------------------------------------------------------------------->
                            <div ng-if="dataTabs.choose === 'shipment'">
                                <form ng-submit="saveShipment($event)">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="">Дата приема товара</label>
                                            <datepicker date-format="dd.MM.yyyy">
                                                <input class="input" type="text" ng-model="data.forms.shipment.date">
                                            </datepicker>
                                            <div validation-error="data.validate.forms.shipment.date" ng-model="data.forms.shipment.date" show-error="data.validate.show"></div>
                                        </div>
                                        <div class="col-md-4">
                                            <label for="">Адрес склада</label>
                                            <input class="input" type="text" ng-model="data.forms.shipment.address">
                                            <div validation-error="data.validate.forms.shipment.address" ng-model="data.forms.shipment.address" show-error="data.validate.show"></div>
                                        </div>
                                        <div class="col-md-4">
                                            <label for="">Ответственный</label>
                                            <input class="input" type="text" ng-model="data.forms.shipment.responsible">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label for="">Комментарий</label>
                                            <textarea class="input textarea" ng-model="data.forms.shipment.comment"></textarea>
                                        </div>
                                        <div class="col-md-12">
                                            <label for="">Дополнительные складские услуги</label>
                                            <textarea class="input textarea" ng-model="data.forms.shipment.additionalServices"></textarea>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table class="table table-striped table-bordered detail-view" style="margin-top: 20px;">
                                                <colgroup>
                                                    <col width="">
                                                    <col width="120px">
                                                    <col width="180px">
                                                    <col width="120px">
                                                    <col width="120px">
                                                    <col width="30px">
                                                </colgroup>
                                                <thead class="thead-default">
                                                <tr>
                                                    <th>Товар</th>
                                                    <th>Ед.изм.</th>
                                                    <th>Цена</th>
                                                    <th>Кол-во*</th>
                                                    <th>Статус</th>
                                                    <th></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr ng-repeat="(productID, product) in data.forms.shipment.products track by $index">
                                                    <td>{{data.objLists.products[product.id].name}}</td>
                                                    <td>{{product.unit || data.objLists.products[product.id].unit}}</td>
                                                    <td>{{product.price || data.objLists.products[product.id].price}}</td>
                                                    <td>{{product.quantity}}</td>
                                                    <td>{{data.objLists.productStatuses[product.status]}}</td>
                                                    <td><a ng-click="removeProduct(data.forms.shipment.products, productID)"><i class="glyphicon glyphicon-remove"></i></a></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <div validation-error="data.validate.forms.shipment.products" ng-model="data.forms.shipment.products" show-error="data.validate.show"></div>
                                            <a class="btn btn-default pull-right" ng-click="productWindow.open()">Добавить товар</a>

                                        </div>
                                    </div>

                                    <div class="row" style="margin-top: 20px;">
                                        <div class="col-md-12">
                                            <button type="submit" class="btn btn-primary pull-right" ng-click="saveShipment($event)">Отправить заявку</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!--------------------------------------------------------------------------------------------------------------->
                            <div ng-if="dataTabs.choose === 'doc'">
                                <form ng-submit="saveDocuments($event)">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="">Тип документа</label>
                                            <div
                                                    special-select
                                                    ng-model="data.forms.doc.type"
                                                    choose-option-id="data.forms.doc.chooseId"
                                                    select-option="data.lists.docTypes"
                                                    choose-option="data.forms.doc.docType"
                                                    instead-key="key"
                                                    instead-value="['value']"
                                                    empty-row="Выбрать тип"
                                                    empty-value-text="Тип"
                                            ></div>
                                            <div validation-error="data.validate.forms.doc.type" ng-model="data.forms.doc.type" show-error="data.validate.show"></div>
                                        </div>
                                        <div class="col-md-8">
                                            <label for="">Дата</label>
                                            <datepicker date-format="dd.MM.yyyy">
                                                <input class="input" type="text" ng-model="data.forms.doc.date">
                                            </datepicker>
                                            <div validation-error="data.validate.forms.doc.date" ng-model="data.forms.doc.date" show-error="data.validate.show"></div>
                                        </div>
                                        <div class="col-md-12" style="margin-top: 15px;">
                                            <div ng-if="data.forms.doc.attachment" style="border-bottom: 1px solid #ddd">
                                                <label for="attachment"> Добавлено вложение {{data.forms.doc.attachment.name}}</label>
                                                <a class="pull-right" ng-click="data.forms.doc.attachment = null"><i class="glyphicon glyphicon-remove"></i>Удалить вложение</a>
                                            </div>
                                            <div ng-if="!data.forms.doc.attachment">
                                                <label for="attachment" class="btn btn-default">Выбрать вложение</label>
                                                <input id="attachment" type="file" class="hidden_file_input" onchange="angular.element(this).scope().getFile(event)" style="display: none;">
                                                <div validation-error="data.validate.forms.doc.attachment" ng-model="data.forms.doc.attachment" show-error="data.validate.show"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-top: 10px">
                                        <div class="col-md-12">
                                            <button type="submit" class="btn btn-primary pull-right" ng-click="saveDocuments($event)">Загрузить документ</button>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table class="table table-striped table-bordered detail-view" style="margin-top: 20px;">
                                                <colgroup>
                                                    <col width="">
                                                </colgroup>
                                                <thead class="thead-default">
                                                <tr>
                                                    <th>Наименование</th>
                                                    <th>Дата</th>
                                                    <th>Тип</th>
                                                    <th>Статус</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr ng-repeat="(docID, doc) in data.forms.doc.list track by $index">
                                                    <td>{{doc.name}}</td>
                                                    <td>{{doc.date}}</td>
                                                    <td>{{data.objLists.docTypes[doc.type]}}</td>
                                                    <td>{{data.objLists.docStatuses[doc.status]}}</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!--------------------------------------------------------------------------------------------------------------->
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
