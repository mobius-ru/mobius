<?php

use app\models\User;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::$app->user->identity->role_id === User::ROLE_ADMIN ? 'Пользователи':'Клиенты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать пользователя', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'company_name','label' => 'Компания', 'value'=>'company.name'],
            ['attribute' => 'company_inn','label' => 'ИНН', 'value'=>'company.inn'],
            'fio',
            'email:email',
//            ['attribute' => 'role_name','label' => 'Роль', 'value'=>'role.name', 'filter' => User::$roles],
            [
                'attribute' => 'role_name',
                'label' => 'Роль',
                'value' => function ($model) { return User::$roles[$model->role_id]; },
                'filter' => User::$roles
            ],
            //'status',
            //'_1C_guid',
            //'created_at',
            //'role_id',
            //'updated_at',
            //'auth_key',
            //'password_hash',
            //'password_reset_token',
            ['class' => 'yii\grid\ActionColumn', 'template' => '{view}'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
