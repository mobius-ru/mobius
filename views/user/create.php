<?php

use app\models\User;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = 'Добавление '.(Yii::$app->user->identity->role_id == User::ROLE_ADMIN ? 'пользователя':'клиента');
$this->params['breadcrumbs'][] = ['label' => Yii::$app->user->identity->role_id === User::ROLE_ADMIN ? 'Пользователи':'Клиенты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
