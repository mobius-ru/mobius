<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>


    <?= $form->field($model, 'role_id')->widget(\kartik\select2\Select2::classname(), [
        'data' => \app\models\User::$roles,
        'options' => ['placeholder' => 'Выберите роль ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
        'pluginEvents' => [
            "change" => "function(e) {
                if(e.target.value != 3) {
                    $('.field-user-contract_number').hide();
                } else {
                    $('.field-user-contract_number').show();
                }
            }",
        ]
    ]);?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'contract_number')->textInput() ?>


    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
