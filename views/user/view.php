<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = $model->company ? $model->fio . " (".$model->company->name.")" : $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить этот договор?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'id',
            'fio',
            ['attribute' => 'company.name', 'label' => 'Компания'],
            'email:email',
            ['attribute' => 'role.name', 'label' => 'Роль'],
            'created_at',
            'updated_at',
//            'auth_key',
//            'password_hash',
//            'password_reset_token',
//            'status',
//            '_1C_guid',
        ],
    ]) ?>

    <?php if($model->company):?>
    <p>
        <?= Html::a('Перейти к договорам пользователя', \yii\helpers\Url::toRoute(['contract/index', 'ContractSearch[company_inn]' => $model->company->inn]), ['class' => 'btn btn-primary']) ?>
    </p>
    <?php endif; ?>

</div>
