<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Document */
/* @var $contractModel app\models\Contract */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="document-form">

    <?php $form = ActiveForm::begin([
            'id' => 'file_form',
            'action' => ['documents/create'],

            'options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'type')->widget(\kartik\select2\Select2::classname(), [
        'data' => \app\models\Document::$types,
        'options' => ['placeholder' => 'Выберите тип документа ...'],
        'pluginOptions' => [
            'allowClear' => true
        ]
    ]);?>

    <?= $form->field($model, 'documentFile')->fileInput() ?>

    <?= $form->field($model, 'contract_id')->hiddenInput(['value' => $contractModel->id])->label(false) ?>

    <div class="form-group">
        <button type="button" class="btn btn-success" id="file_form_submit">Upload</button>

    </div>

    <?php ActiveForm::end(); ?>

    <script>
        setTimeout(function () {
            $('#file_form').on('beforeSubmit', function () {
                // Вызывается после удачной валидации всех полей и до того как форма отправляется на северер.
                // Тут можно отправить форму через AJAX. Не забудьте вернуть false для того, чтобы форма не отправлялась как обычно.
            });
        }, 0)

    </script>

</div>


