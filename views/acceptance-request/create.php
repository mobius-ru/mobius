<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AcceptanceRequest */

$this->title = 'Create Acceptance Request';
$this->params['breadcrumbs'][] = ['label' => 'Acceptance Requests', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="acceptance-request-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
