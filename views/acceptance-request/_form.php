<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\AcceptanceRequest */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="acceptance-request-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'date')->widget(kartik\datetime\DateTimePicker::classname(), [
        'options' => ['placeholder' => $model->attributeLabels()['date']],
        'pluginOptions' => [
            'autoclose' => true
        ]
    ]);?>

    <?= $form->field($model, 'contract_id')->hiddenInput(['value' => $contractModel->id])->label(false) ?>

    <?= $form->field($model, 'address')->textInput(['maxlength' => true, 'placeholder' => $model->attributeLabels()['address']]) ?>

    <?= $form->field($model, 'comment_message')->textarea(['maxlength' => true]) ?>

    <?= $form->field($model, 'additional_services')->textarea(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>



</div>
