<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ShipmentRequest */

$this->title = 'Create Shipment Request';
$this->params['breadcrumbs'][] = ['label' => 'Shipment Requests', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shipment-request-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
