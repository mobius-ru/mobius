<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "acceptance_requests".
 *
 * @property int $id
 * @property string $date Дата приема товара
 * @property int $status Статус
 * @property int $contract_id Договр
 * @property string $address Адоес склада
 * @property string|null $responsible Ответственный
 * @property string|null $comment_message Комментарий
 * @property string|null $additional_services Дополнительные складские услуги
 *
 * @property AcceptanceRequestGoods[] $acceptanceRequestGoods
 * @property Contract $contract
 */
class AcceptanceRequest extends \yii\db\ActiveRecord
{
    const STATUS_DRAFT = 1;
    const STATUS_SENT = 2;
    const STATUS_CLOSED = 3;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'acceptance_requests';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date', 'contract_id', 'address'], 'required'],
            [['date', 'responsible'], 'safe'],
            [['contract_id'], 'default', 'value' => null],
            [['status'], 'default', 'value' => self::STATUS_DRAFT],
            [['status', 'contract_id'], 'integer'],
            [['address', 'comment_message', 'additional_services'], 'string', 'max' => 255],
            [['contract_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contract::className(), 'targetAttribute' => ['contract_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Дата приема товара',
            'status' => 'Статус',
            'contract_id' => 'Договор',
            'address' => 'Адрес склада',
            'comment_message' => 'Комментарий',
            'additional_services' => 'Дополнительные складские услуги',
            'responsible' => 'Ответственный'
        ];
    }

    /**
     * Gets query for [[AcceptanceRequestGoods]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAcceptanceRequestGoods()
    {
        return $this->hasMany(AcceptanceRequestGoods::className(), ['acceptance_request_id' => 'id']);
    }

    /**
     * Gets query for [[Contract]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getContract()
    {
        return $this->hasOne(Contract::className(), ['id' => 'contract_id']);
    }
}
