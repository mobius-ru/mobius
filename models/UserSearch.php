<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\User;

/**
 * UserSearch represents the model behind the search form of `app\models\User`.
 */
class UserSearch extends User
{
    public $role_name;
    public $company_inn;
    public $company_name;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'status', 'role_id'], 'integer'],
            [['auth_key', 'password_hash', 'password_reset_token', 'email', 'fio', '_1C_guid', 'created_at', 'updated_at', 'role_name', 'company_name', 'company_inn'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */

    public function search($params)
    {



        $query = User::find();

        $query->joinWith(['role']);
        $query->joinWith(['company']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['role_name'] = [
            'asc' => [Role::tableName().'.name' => SORT_ASC],
            'desc' => [Role::tableName().'.name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['company_name'] = [
            'asc' => [Company::tableName().'.name' => SORT_ASC],
            'desc' => [Company::tableName().'.name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['company_inn'] = [
            'asc' => [Company::tableName().'.inn' => SORT_ASC],
            'desc' => [Company::tableName().'.inn' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'role_id' => $this->role_id,
            'updated_at' => $this->updated_at,
            'inn' => $this->company->inn ?? null,
            'companyName' => $this->company->name ?? null
        ]);


        $query->andFilterWhere(['like', 'auth_key', $this->auth_key])
            ->andFilterWhere(['like', 'password_hash', $this->password_hash])
            ->andFilterWhere(['like', 'password_reset_token', $this->password_reset_token])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'fio', $this->fio])
            ->andFilterWhere(['like', Company::tableName().'.name', $this->company_name])
            ->andFilterWhere(['like', Company::tableName().'.inn', $this->company_inn])
            ->andFilterWhere(['like', '_1C_guid', $this->_1C_guid]);

        $query->andFilterWhere(['=', Role::tableName().'.id', $this->role_name]);

        return $dataProvider;
    }
}
