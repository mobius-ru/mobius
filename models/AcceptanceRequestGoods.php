<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "acceptance_request_goods".
 *
 * @property int $acceptance_request_id Заявка на прием товара
 * @property int $goods_id Товар
 * @property float $count Количество
 * @property int $status Статус
 *
 * @property AcceptanceRequest $acceptanceRequest
 * @property Goods $goods
 */
class AcceptanceRequestGoods extends \yii\db\ActiveRecord
{
    const STATUS_OK = 1;
    const STATUS_FAIL = 2;

    public static $statuses = [
        self::STATUS_OK => 'Успешно',
        self::STATUS_FAIL => 'Не успешно',
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'acceptance_request_goods';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['acceptance_request_id', 'goods_id', 'count'], 'required'],
            [['acceptance_request_id', 'goods_id'], 'default', 'value' => null],
            [['status'], 'default', 'value' => self::STATUS_OK],
            [['acceptance_request_id', 'goods_id', 'status'], 'integer'],
            [['count'], 'number'],
            [['acceptance_request_id'], 'exist', 'skipOnError' => true, 'targetClass' => AcceptanceRequest::className(), 'targetAttribute' => ['acceptance_request_id' => 'id']],
            [['goods_id'], 'exist', 'skipOnError' => true, 'targetClass' => Goods::className(), 'targetAttribute' => ['goods_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'acceptance_request_id' => 'Заявка на прием товара',
            'goods_id' => 'Товар',
            'count' => 'Количество',
            'status' => 'Статус',
        ];
    }

    /**
     * Gets query for [[AcceptanceRequest]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAcceptanceRequest()
    {
        return $this->hasOne(AcceptanceRequest::className(), ['id' => 'acceptance_request_id']);
    }

    /**
     * Gets query for [[Goods]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGoods()
    {
        return $this->hasOne(Goods::className(), ['id' => 'goods_id']);
    }
}
