<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "contracts".
 *
 * @property int $id
 * @property string $number Номер договора
 * @property string $date_of_conclusion Дата заключения договора
 * @property int $status Статус
 * @property int $user_id Клиент
 *
 * @property AcceptanceRequest[] $acceptanceRequests
 * @property AcceptanceRequest $acceptanceRequest
 * @property User $user
 * @property Document[] $documents
 * @property ShipmentRequest[] $shipmentRequests
 * @property ShipmentRequest $shipmentRequest
 */
class Contract extends \yii\db\ActiveRecord
{
    const STATUS_NEW = 0;
    const STATUS_OPEN = 1;
    const STATUS_PROCESSING = 2;
    const STATUS_CLOSED = 3;

    public static $statuses = [
        self::STATUS_NEW => 'Заключен',
        self::STATUS_OPEN => 'Завершен',//'Заключен', //todo выяснить инфу по статусам
        self::STATUS_PROCESSING => 'В работе',
        self::STATUS_CLOSED => 'Завершен',
    ];
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contracts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['number', 'date_of_conclusion', 'user_id'], 'required'],
            [['date_of_conclusion'], 'safe'],
            [['status', 'user_id'], 'default', 'value' => null],
            [['status', 'user_id'], 'integer'],
            [['number'], 'string', 'max' => 255],
            [['number'], 'unique'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'number' => 'Номер договора',
            'date_of_conclusion' => 'Дата заключения договора',
            'status' => 'Статус',
            'user_id' => 'Клиент',
        ];
    }

    /**
     * Gets query for [[AcceptanceRequests]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAcceptanceRequests()
    {
        return $this->hasMany(AcceptanceRequest::className(), ['contract_id' => 'id']);
    }

    /**
     * Gets query for AcceptanceRequests.
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAcceptanceRequest()
    {
        return $this->hasOne(AcceptanceRequest::className(), ['contract_id' => 'id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * Gets query for [[Documents]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDocuments()
    {
        return $this->hasMany(Document::className(), ['contract_id' => 'id']);
    }

    /**
     * Gets query for [[ShipmentRequests]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getShipmentRequests()
    {
        return $this->hasMany(ShipmentRequest::className(), ['contract_id' => 'id']);
    }

    /**
     * Gets query for [[ShipmentRequests]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getShipmentRequest()
    {
        return $this->hasOne(ShipmentRequest::className(), ['contract_id' => 'id']);
    }
}
