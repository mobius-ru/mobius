<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "shipment_requests".
 *
 * @property int $id
 * @property string $date Дата доставки
 * @property int $status Статус
 * @property int $contract_id Договор
 * @property string $address Адрес доставки
 * @property string|null $responsible Ответственный
 * @property string|null $comment_message Комментарий
 * @property string|null $additional_services Дополнительные склаские услуги
 *
 * @property ShipmentRequestGoods[] $shipmentRequestGoods
 * @property Contract $contract
 */
class ShipmentRequest extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'shipment_requests';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date', 'contract_id', 'address'], 'required'],
            [['date', 'responsible'], 'safe'],
            [['status', 'contract_id'], 'default', 'value' => null],
            [['status', 'contract_id'], 'integer'],
            [['address', 'comment_message', 'additional_services'], 'string', 'max' => 255],
            [['contract_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contract::className(), 'targetAttribute' => ['contract_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Дата доставки',
            'status' => 'Статус',
            'contract_id' => 'Договор',
            'address' => 'Адрес доставки',
            'comment_message' => 'Комментарий',
            'additional_services' => 'Дополнительные склаские услуги',
            'responsible' => 'Ответственный'
        ];
    }

    /**
     * Gets query for [[ShipmentRequestGoods]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getShipmentRequestGoods()
    {
        return $this->hasMany(ShipmentRequestGoods::className(), ['shipment_requests_id' => 'id']);
    }

    /**
     * Gets query for [[Contract]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getContract()
    {
        return $this->hasOne(Contract::className(), ['id' => 'contract_id']);
    }
}
