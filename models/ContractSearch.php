<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Contract;

/**
 * ContractSearch represents the model behind the search form of `app\models\Contract`.
 */
class ContractSearch extends Contract
{
    public $user_name;
    public $company_name;
    public $company_inn;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'status', 'user_id'], 'integer'],
            [['number', 'date_of_conclusion', 'user_name', 'company_name', 'company_inn'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Contract::find();

        $query->joinWith(['user']);
        $query->joinWith(['user.companies']);


        // add conditions that should always apply here
        if(Yii::$app->user->identity->role_id == User::ROLE_CLIENT) {
            $query->where(['user_id' => Yii::$app->user->identity->getId()]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['user_name'] = [
            'asc' => [User::tableName().'.fio' => SORT_ASC],
            'desc' => [User::tableName().'.fio' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['company_name'] = [
            'asc' => [Company::tableName().'.name' => SORT_ASC],
            'desc' => [Company::tableName().'.name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['company_inn'] = [
            'asc' => [Company::tableName().'.inn' => SORT_ASC],
            'desc' => [Company::tableName().'.inn' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'date_of_conclusion' => $this->date_of_conclusion,
            'contracts.status' => $this->status,
            'user_id' => $this->user_id,
        ]);

        $query->andFilterWhere(['like', 'number', $this->number]);
        $query->andFilterWhere(['like', User::tableName().'.fio', $this->user_name]);
        $query->andFilterWhere(['like', Company::tableName().'.name', $this->company_name]);
        $query->andFilterWhere(['like', Company::tableName().'.inn', $this->company_inn]);

        return $dataProvider;
    }
}
