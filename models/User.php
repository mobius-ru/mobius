<?php

namespace app\models;

use app\services\_1C_SoapEndpoint;
use app\services\SoapDto;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $auth_key
 * @property string $password_hash Пароль
 * @property string|null $password_reset_token
 * @property string $email Email
 * @property int $status Статус пользователя
 * @property string|null $fio ФИО
 * @property string|null $_1C_guid Идентификатор пользователя в 1С
 * @property int $created_at Создан
 * @property int|null $role_id Роль пользователя
 * @property int $updated_at
 *
 * @property Company[] $companies
 * @property Company $company
 * @property Contract[] $contracts
 * @property FeedbackRequest[] $feedbackRequests
 * @property Goods[] $goods
 * @property Role $role
 */
class User extends \yii\db\ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;

    const ROLE_ADMIN = 1;
    const ROLE_MANAGER = 2;
    const ROLE_CLIENT = 3;

    public static $roles = [
        self::ROLE_ADMIN => 'Администратор',
        self::ROLE_MANAGER => 'Менеджер',
        self::ROLE_CLIENT => 'Клиент',
    ];

    public $contract_number;
    public $password;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email', 'role_id'], 'required'],
            [['email'], 'email'],
            [['contract_number'], 'required', 'when' => function ($model) {
                return $model->role_id == self::ROLE_CLIENT;
            }, 'whenClient' => "function (attribute, value) {
                return $('#user-role_id').val() == 3;
            }"],
            [['role_id', 'updated_at'], 'default', 'value' => null],
            [['created_at','updated_at'],'default', 'value' => date('Y-m-d H:i:s')],
            [['status', 'role_id'], 'integer'],
            [['created_at', 'updated_at'], 'datetime', 'format' => 'php:Y-m-d H:i:s'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            [['auth_key'], 'string', 'max' => 32],
            [['password_hash', 'password_reset_token', 'email', 'fio', '_1C_guid'], 'string', 'max' => 255],
            [['_1C_guid'], 'unique'],
            [['email'], 'unique'],
            [['password_reset_token'], 'unique'],
            [['role_id'], 'exist', 'skipOnError' => true, 'targetClass' => Role::className(), 'targetAttribute' => ['role_id' => 'id']],
        ];
    }


    /**
     * @param bool $insert
     * @return bool
     * @throws \yii\base\Exception
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->auth_key = Yii::$app->security->generateRandomString();
                $this->password = Yii::$app->security->generateRandomString();
                $this->password_hash = Yii::$app->security->generatePasswordHash($this->password);
            }
            $this->created_at = date('Y-m-d H:i:s', $this->created_at);
            $this->updated_at = date('Y-m-d H:i:s', $this->updated_at);
            return true;
        }
        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Пароль',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'Email',
            'status' => 'Статус пользователя',
            'fio' => 'ФИО',
            '_1C_guid' => 'Идентификатор пользователя в 1С',
            'created_at' => 'Создан',
            'role_id' => 'Роль пользователя',
            'updated_at' => 'Обновлен',
            'contract_number' => 'Номер договора'
        ];
    }

    /**
     * Gets query for [[Companies]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompanies()
    {
        return $this->hasMany(Company::className(), ['user_id' => 'id']);
    }

    /**
     * Gets query for Company.
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['user_id' => 'id']);
    }

    /**
     * Gets query for [[Contracts]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getContracts()
    {
        return $this->hasMany(Contract::className(), ['user_id' => 'id']);
    }

    /**
     * Gets query for [[FeedbackRequests]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFeedbackRequests()
    {
        return $this->hasMany(FeedbackRequest::className(), ['user_id' => 'id']);
    }

    /**
     * Gets query for [[Goods]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGoods()
    {
        return $this->hasMany(Goods::className(), ['user_id' => 'id']);
    }

    /**
     * Gets query for [[Role]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRole()
    {
        return $this->hasOne(Role::className(), ['id' => 'role_id']);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['email' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**  * @inheritdoc  */
    public static function findIdentity($id) {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**  * @inheritdoc  */
    public static function findIdentityByAccessToken($token, $type = null) {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**  * @inheritdoc  */
    public function getId() {
        return $this->getPrimaryKey();
    }

    /**  * @inheritdoc  */
    public function getAuthKey() {
        return $this->auth_key;
    }

    /**  * @inheritdoc  */
    public function validateAuthKey($authKey) {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     * @throws \yii\base\Exception
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * @param string $message
     * @param string $mailSubject
     * @param array $attach
     * @param string $from
     * @return bool
     */
    public function sendMail($message, $mailSubject = '', $attach = [], $from = null){
        $mailer = Yii::$app->mailer->compose();
        if ($attach){
            foreach ($attach as $file){
                $mailer->attachContent(base64_decode($file->data)?base64_decode($file->data):$file->data, ['fileName' => $file->nameWithExtention(), 'contentType' => 'text/plain']);
            }
        }
        return $mailer->setFrom($from ? $from : Yii::$app->params['senderEmail'])
            ->setTo($this->email)
            ->setSubject($mailSubject)
            // ->setTextBody($message)
            ->setHtmlBody($message)
            ->send();

    }
}
