<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "shipment_request_goods".
 *
 * @property int $shipment_requests_id Заявка на отгрузку
 * @property int $goods_id Товар
 * @property float $count Количество
 * @property int $status Статус
 *
 * @property Goods $goods
 * @property ShipmentRequest $shipmentRequests
 */
class ShipmentRequestGoods extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'shipment_request_goods';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['shipment_requests_id', 'goods_id', 'count'], 'required'],
            [['shipment_requests_id', 'goods_id', 'status'], 'default', 'value' => null],
            [['shipment_requests_id', 'goods_id', 'status'], 'integer'],
            [['count'], 'number'],
            [['goods_id'], 'exist', 'skipOnError' => true, 'targetClass' => Goods::className(), 'targetAttribute' => ['goods_id' => 'id']],
            [['shipment_requests_id'], 'exist', 'skipOnError' => true, 'targetClass' => ShipmentRequest::className(), 'targetAttribute' => ['shipment_requests_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'shipment_requests_id' => 'Заявка на отгрузку',
            'goods_id' => 'Товар',
            'count' => 'Количество',
            'status' => 'Статус',
        ];
    }

    /**
     * Gets query for [[Goods]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGoods()
    {
        return $this->hasOne(Goods::className(), ['id' => 'goods_id']);
    }

    /**
     * Gets query for [[ShipmentRequests]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getShipmentRequests()
    {
        return $this->hasOne(ShipmentRequest::className(), ['id' => 'shipment_requests_id']);
    }
}
