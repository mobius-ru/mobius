<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "documents".
 *
 * @property int $id
 * @property string $path Путь
 * @property string $name Имя
 * @property string $date Дата
 * @property int $type Тип документа
 * @property int $contract_id Договор
 * @property int $status Статус
 *
 * @property Contract $contract
 */
class Document extends \yii\db\ActiveRecord
{
    const STATUS_NEW = 1;
    const STATUS_APPROVED = 2;
    const STATUS_DECLINED = 3;

    const TYPE_ACCEPTANCE_REQUEST = 1;
    const TYPE_SHIPMENT_REQUEST = 2;
    const TYPE_MX1 = 3;
    const TYPE_MX3 = 4;
    const TYPE_INVOICE = 5;
    const TYPE_PACKING_LIST = 6;
    const TYPE_CUSTOM = 7;

    public static $types = [
        self::TYPE_ACCEPTANCE_REQUEST => 'Заявка на выгрузку',
        self::TYPE_SHIPMENT_REQUEST => 'Заявка на отгрузку',
        self::TYPE_MX1 => 'Документ МХ1',
        self::TYPE_MX3 => 'Документ МХ3',
        self::TYPE_INVOICE => 'Счёт-фактура',
        self::TYPE_INVOICE => 'Товарная накладная',
        self::TYPE_INVOICE => 'Иные документы',
    ];

    public static $statuses = [
        self::STATUS_NEW => 'Новый',
        self::STATUS_APPROVED => 'Подтвержден',
        self::STATUS_DECLINED => 'Отклонен',
    ];

    public $file;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'documents';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['file'], 'file', 'skipOnEmpty' => true],
            [['date'], 'safe'],
            [['type', 'contract_id', 'path', 'name'], 'required'],
            [['type', 'contract_id', 'status'], 'default', 'value' => self::STATUS_NEW],
            [['type', 'contract_id', 'status'], 'integer'],
            [['contract_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contract::className(), 'targetAttribute' => ['contract_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Тип документа',
            'contract_id' => 'Договор',
            'status' => 'Статус',
            'path' => 'Путь',
            'name' => 'Имя',
            'date' => 'Дата'
        ];
    }

    /**
     * Gets query for [[Contract]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getContract()
    {
        return $this->hasOne(Contract::className(), ['id' => 'contract_id']);
    }
}
