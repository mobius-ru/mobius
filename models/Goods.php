<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "goods".
 *
 * @property int $id
 * @property int $user_id Клиент
 * @property int|null $contract_id Договор
 * @property float|null $price Цена
 * @property string|null $unit Единица измерения
 * @property float|null $balance Остаток
 * @property string $name Наименование
 *
 * @property AcceptanceRequestGoods[] $acceptanceRequestGoods
 * @property User $user
 * @property ShipmentRequestGoods[] $shipmentRequestGoods
 */
class Goods extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'goods';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'name'], 'required'],
            [['user_id', 'contract_id'], 'default', 'value' => null],
            [['user_id', 'contract_id'], 'integer'],
            [['price', 'balance'], 'number'],
            [['unit', 'name'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Клиент',
            'contract_id' => 'Договор',
            'price' => 'Цена',
            'unit' => 'Единица измерения',
            'balance' => 'Остаток',
            'name' => 'Наименование',
        ];
    }

    /**
     * Gets query for [[AcceptanceRequestGoods]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAcceptanceRequestGoods()
    {
        return $this->hasMany(AcceptanceRequestGoods::className(), ['goods_id' => 'id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * Gets query for [[ShipmentRequestGoods]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getShipmentRequestGoods()
    {
        return $this->hasMany(ShipmentRequestGoods::className(), ['goods_id' => 'id']);
    }
}
