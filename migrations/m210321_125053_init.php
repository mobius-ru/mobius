<?php

use app\models\User;
use yii\db\Migration;

/**
 * Class m210321_125053_init
 */
class m210321_125053_init extends Migration
{
    /**
     * {@inheritdoc}
     * @throws \yii\base\Exception
     */
    public function safeUp()
    {

        $tableOptions = null;
        if($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci 
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }


        $this->createTable('{{%users}}', [
            'id' => $this->primaryKey(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull()->comment('Пароль'),
            'password_reset_token' => $this->string()->unique(),
            'email' => $this->string()->notNull()->unique()->comment('Email'),
            'status' => $this->smallInteger()->notNull()->defaultValue(10)->comment('Статус пользователя'),
            'fio' => $this->string()->comment('ФИО'),
            '_1C_guid' => $this->string()->unique()->comment('Идентификатор пользователя в 1С'),
            'created_at' => $this->dateTime()->notNull()->comment('Создан'),
            'role_id' => $this->integer()->comment('Роль пользователя'),
            'updated_at' => $this->dateTime(),
            ], $tableOptions);

        $this->createTable('{{%roles}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()->comment('Наименование роли'),
        ], $tableOptions);

        $this->createTable('{{%goods}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->comment('Клиент'),
            'contract_id' => $this->integer()->comment('Договор'),
            'price' => $this->float()->comment('Цена'),
            'unit' => $this->string()->comment('Единица измерения'),
            'balance' => $this->float()->comment('Остаток'),
            'name' => $this->string()->notNull()->comment('Наименование'),
        ], $tableOptions);

        $this->createTable('{{%contracts}}', [
            'id' => $this->primaryKey(),
            'number' => $this->string()->notNull()->unique()->comment('Номер договора'),
            'date_of_conclusion' => $this->date()->notNull()->comment('Дата заключения договора'),
            'status' => $this->smallInteger()->notNull()->defaultValue(1)->comment('Статус'),
            'user_id' => $this->integer()->comment('Клиент')
        ], $tableOptions);

        $this->createTable('{{%shipment_requests}}', [
            'id' => $this->primaryKey(),
            'date' => $this->date()->notNull()->comment('Дата доставки'),
            'status' => $this->smallInteger()->notNull()->defaultValue(1)->comment('Статус'),
            'contract_id' => $this->integer()->comment('Договор'),
            'address' => $this->string()->notNull()->comment('Адрес доставки'),
            'comment_message' => $this->string()->comment('Комментарий'),
            'additional_services' => $this->string()->comment('Дополнительные склаские услуги'),
        ], $tableOptions);

        $this->createTable('{{%shipment_request_goods}}', [
            'shipment_request_id' => $this->integer()->comment('Заявка на отгрузку'),
            'goods_id' => $this->integer()->comment('Товар'),
            'count' => $this->float()->notNull()->comment('Количество'),
            'status' => $this->smallInteger()->notNull()->defaultValue(1)->comment('Статус'),
        ], $tableOptions);

        $this->createTable('{{%acceptance_requests}}', [
            'id' => $this->primaryKey(),
            'date' => $this->date()->notNull()->comment('Дата приема товара'),
            'status' => $this->smallInteger()->notNull()->defaultValue(1)->comment('Статус'),
            'contract_id' => $this->integer()->comment('Договор'),
            'address' => $this->string()->notNull()->comment('Адрес склада'),
            'comment_message' => $this->string()->comment('Комментарий'),
            'additional_services' => $this->string()->comment('Дополнительные складские услуги'),
        ], $tableOptions);

        $this->createTable('{{%acceptance_request_goods}}', [
            'acceptance_request_id' => $this->integer()->comment('Заявка на прием товара'),
            'goods_id' => $this->integer()->comment('Товар'),
            'count' => $this->float()->notNull()->comment('Количество'),
            'status' => $this->smallInteger()->notNull()->defaultValue(1)->comment('Статус'),
        ], $tableOptions);

        $this->createTable('{{%documents}}', [
            'id' => $this->primaryKey(),
            'path' => $this->string()->notNull()->comment('Путь'),
            'type' => $this->smallInteger()->notNull()->comment('Тип документа'),
            'contract_id' => $this->integer()->comment('Договор'),
            'status' => $this->smallInteger()->notNull()->defaultValue(1)->comment('Статус'),
        ], $tableOptions);

        $this->createTable('{{%companies}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->comment('Клиент'),
            'name' => $this->string()->notNull()->comment('Наименование компании'),
            'inn' => $this->string(12)->notNull()->comment('ИНН'),
            'kpp' => $this->string(9)->comment('КПП'),
            'ogrn' => $this->string(15)->notNull()->comment('ОГРН/ОГРНИП'),
        ], $tableOptions);

        $this->createTable('{{%feedback_requests}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->comment('Клиент'),
            'title' => $this->string()->notNull()->comment('Тема'),
            'message' => $this->string(12)->notNull()->comment('Сообщение'),
            'status' => $this->smallInteger()->notNull()->defaultValue(1)->comment('Статус'),
        ], $tableOptions);

        $this->addPrimaryKey('pk-acceptance_request_goods','{{%acceptance_request_goods}}',['acceptance_request_id','goods_id']);
        $this->addPrimaryKey('pk-shipment_request_goods','{{%shipment_request_goods}}',['shipment_request_id','goods_id']);
        $this->createIndex('idx-user-role_id', '{{%users}}', 'role_id');
        $this->addForeignKey('fk-user-role', '{{%users}}', 'role_id', '{{%roles}}', 'id', 'SET NULL', 'RESTRICT');
        $this->createIndex('idx-goods-user_id', '{{%goods}}', 'user_id');
        $this->createIndex('idx-goods-contract_id', '{{%goods}}', 'contract_id');
        $this->addForeignKey('fk-goods-user', '{{%goods}}', 'user_id', '{{%users}}', 'id', 'SET NULL', 'RESTRICT');
        $this->createIndex('idx-contracts-user_id', '{{%contracts}}', 'user_id');
        $this->addForeignKey('fk-contracts-user', '{{%contracts}}', 'user_id', '{{%users}}', 'id', 'SET NULL', 'RESTRICT');
        $this->createIndex('idx-shipment_requests-contract_id', '{{%shipment_requests}}', 'contract_id');
        $this->addForeignKey('fk-shipment_requests-contracts', '{{%shipment_requests}}', 'contract_id', '{{%contracts}}', 'id', 'SET NULL', 'RESTRICT');
        $this->createIndex('idx-shipment_request_goods-shipment_request_id', '{{%shipment_request_goods}}', 'shipment_request_id');
        $this->addForeignKey('fk-shipment_request_goods-shipment_requests', '{{%shipment_request_goods}}', 'shipment_request_id', '{{%shipment_requests}}', 'id', 'SET NULL', 'RESTRICT');
        $this->createIndex('idx-shipment_request_goods-goods_id', '{{%shipment_request_goods}}', 'goods_id');
        $this->addForeignKey('fk-shipment_request_goods-goods', '{{%shipment_request_goods}}', 'goods_id', '{{%goods}}', 'id', 'SET NULL', 'RESTRICT');
        $this->createIndex('idx-acceptance_requests-contract_id', '{{%acceptance_requests}}', 'contract_id');
        $this->addForeignKey('fk-acceptance_requests-contracts', '{{%acceptance_requests}}', 'contract_id', '{{%contracts}}', 'id', 'SET NULL', 'RESTRICT');
        $this->createIndex('idx-acceptance_request_goods-shipment_request_id', '{{%acceptance_request_goods}}', 'acceptance_request_id');
        $this->addForeignKey('fk-acceptance_request_goods-shipment_requests', '{{%acceptance_request_goods}}', 'acceptance_request_id', '{{%acceptance_requests}}', 'id', 'SET NULL', 'RESTRICT');
        $this->createIndex('idx-acceptance_request_goods-goods_id', '{{%acceptance_request_goods}}', 'goods_id');
        $this->addForeignKey('fk-acceptance_request_goods-goods', '{{%acceptance_request_goods}}', 'goods_id', '{{%goods}}', 'id', 'SET NULL', 'RESTRICT');
        $this->createIndex('idx-documents-contract_id', '{{%documents}}', 'contract_id');
        $this->addForeignKey('fk-documents-contract', '{{%documents}}', 'contract_id', '{{%contracts}}', 'id', 'SET NULL', 'RESTRICT');
        $this->createIndex('idx-companies-user_id', '{{%companies}}', 'user_id');
        $this->addForeignKey('fk-companies-user', '{{%companies}}', 'user_id', '{{%users}}', 'id', 'SET NULL', 'RESTRICT');
        $this->createIndex('idx-feedback_requests-user_id', '{{%feedback_requests}}', 'user_id');
        $this->addForeignKey('fk-feedback_requests-user', '{{%feedback_requests}}', 'user_id', '{{%users}}', 'id', 'SET NULL', 'RESTRICT');

        $this->insert('{{%roles}}',[
            'name' => User::$roles[User::ROLE_ADMIN],
        ]);

        $this->insert('{{%roles}}',[
            'name' => User::$roles[User::ROLE_MANAGER],
        ]);

        $this->insert('{{%roles}}',[
            'name' => User::$roles[User::ROLE_CLIENT],
        ]);

        $adminUser = new User();
        $adminUser->setPassword(Yii::$app->params['adminDefaultPassword']);
        $this->insert('{{%users}}',[
            'auth_key' => Yii::$app->security->generateRandomString(),
            'password_hash' => $adminUser->password_hash,
            'email' => Yii::$app->params['adminEmail'],
            'status' => User::STATUS_ACTIVE,
            'created_at' => date('Y-m-d H:i:s'),
            'role_id' => User::ROLE_ADMIN,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {


        $this->dropIndex('idx-user-role_id','{{%user}}');
        $this->dropIndex('idx-goods-user_id','{{%goods}}');
        $this->dropIndex('idx-goods-contract_id','{{%goods}}');
        $this->dropIndex('idx-contracts-user_id','{{%contracts}}');
        $this->dropIndex('idx-shipment_requests-contract_id','{{%shipment_requests}}');
        $this->dropIndex('idx-shipment_request_goods-shipment_request_id','{{%shipment_request_goods}}');
        $this->dropIndex('idx-shipment_request_goods-goods_id','{{%shipment_request_goods}}');
        $this->dropIndex('idx-acceptance_requests-contract_id','{{%acceptance_requests}}');
        $this->dropIndex('idx-acceptance_request_goods-shipment_request_id','{{%acceptance_request_goods}}');
        $this->dropIndex('idx-acceptance_request_goods-goods_id','{{%acceptance_request_goods}}');
        $this->dropIndex('idx-documents-contract_id','{{%documents}}');
        $this->dropIndex('idx-companies-user_id','{{%companies}}');
        $this->dropIndex('idx-feedback_requests-user_id','{{%feedback_requests}}');

        $this->dropForeignKey('fk-user-role', '{{%users}}');
        $this->dropForeignKey('fk-goods-user', '{{%goods}}');
        $this->dropForeignKey('fk-contracts-user', '{{%contracts}}');
        $this->dropForeignKey('fk-shipment_requests-contracts', '{{%shipment_requests}}');
        $this->dropForeignKey('fk-shipment_request_goods-shipment_requests', '{{%shipment_request_goods}}');
        $this->dropForeignKey('fk-shipment_request_goods-goods', '{{%shipment_request_goods}}');
        $this->dropForeignKey('fk-acceptance_requests-contracts', '{{%acceptance_requests}}');
        $this->dropForeignKey('fk-acceptance_request_goods-shipment_requests', '{{%acceptance_request_goods}}');
        $this->dropForeignKey('fk-acceptance_request_goods-goods', '{{%acceptance_request_goods}}');
        $this->dropForeignKey('fk-documents-contract', '{{%documents}}');
        $this->dropForeignKey('fk-companies-user', '{{%companies}}');
        $this->dropForeignKey('fk-feedback_requests-user', '{{%feedback_requests}}');

        $this->dropTable('{{%users}}');
        $this->dropTable('{{%roles}}');
        $this->dropTable('{{%goods}}');
        $this->dropTable('{{%contracts}}');
        $this->dropTable('{{%shipment_requests}}');
        $this->dropTable('{{%shipment_request_goods}}');
        $this->dropTable('{{%acceptance_requests}}');
        $this->dropTable('{{%acceptance_request_goods}}');
        $this->dropTable('{{%documents}}');
        $this->dropTable('{{%companies}}');
        $this->dropTable('{{%feedback_requests}}');

    }

}
