<?php

use yii\db\Migration;

/**
 * Class m210329_175147_add_doc_name
 */
class m210329_175147_add_doc_name extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%documents}}','name', 'string');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%documents}}', 'name');
    }
}
