<?php

use yii\db\Migration;

/**
 * Class m210329_210111_add_doc_date
 */
class m210329_210111_add_doc_date extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%documents}}','date', 'date');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%documents}}', 'date');
    }
}
