<?php

use yii\db\Migration;

/**
 * Class m210329_161722_add_responsible_column
 */
class m210329_161722_add_responsible_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->addColumn('{{%shipment_requests}}','responsible', 'string');
        $this->addColumn('{{%acceptance_requests}}','responsible', 'string');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        $this->dropColumn('{{%shipment_requests}}', 'responsible');
        $this->dropColumn('{{%acceptance_requests}}', 'responsible');
    }
}
