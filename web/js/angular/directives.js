var directives = angular.module('directives', []);

directives.directive('modalWindowLight',['$compile', '$window', function($compile, $window){
    return {
        restrict: "A",
        scope: {
            open: '=',
            windowTitle: '=',
            windowWidth: '=',
            windowStyle: '=',
            buttons:'=', //[{title:'OK', classes: 'btn button_common', click: closeStructureWindow || function(){} }]
            close: '=',
            overlayClass: '='
        },
        transclude: true,
        template:(
            '<div class="pop-up_window_overlay {{overlayClass}}" ng-show="open" window-light-scroll="open">' +
            '<div class="pop-up_window">' + // ng-style="style" заменено на scope.addStyles
            '<div ng-if="open">' +
            //'<div ng-init="openFunc()" element-destroy="closeFunc()"></div>' + // используется в офлайне для скрытия и показа палели кнопок. openFunc closeFunc у них прописаны в контроллере
            '<style>body{overflow: hidden}</style>' +
            '</div>' +
            '<a class="close_btn" ng-click="close ? close() : open = false"><i class="glyphicon glyphicon-remove"></i></a>' +
            '<h4 class="pop-up_window_title" style="cursor: move">{{windowTitle}}</h4>' +
            '<div ng-transclude class="modal_window_container"><div  class="window_content" ng-if="open"></div></div>' + // в нфе внутри еще <div class="modal_window_container"></div>
            '<div class="pop-up_window_buttons" ng-if="buttons">' +
            '<span ng-repeat="(buttonID, button) in buttons track by $index">' +
            '<button class="{{button.classes}}" ng-click="button.click ? button.click() : closeWindow()">{{button.title}}</button>' +
            '</span>' +
            '</div>' +
            '<a class="pop-up_window_resize_btn" style="cursor: nw-resize"></a>' +
            '</div>' +
            '</div>'
        ),
        controller: ['$scope', '$element', function($scope, $element) {
            $scope.closeWindow = $scope.close ? $scope.close : function(){
                $scope.open = false;
            };
            this.$window = angular.element($element[0].getElementsByClassName("pop-up_window")[0]);
        }],
        link: function(scope, element, attr){//pop-up_window_title
            var overlay = element[0].getElementsByClassName('pop-up_window_overlay')[0];
            var window = overlay.getElementsByClassName('pop-up_window')[0];
            var WIDTH = 700;
            var HEIGHT = 600;
            scope.style = JSON.parse(JSON.stringify(scope.windowStyle || {}));//windowStyle может быть пустым

            var width = String(scope.windowWidth || scope.style.width || '');
            var height = String(scope.style.height || '');

            scope.calculateWindowParams = function(windowWidth, windowHeight){
                scope.style.width = scope.state.percentWidth ? windowWidth / 100 * scope.state.percentWidth : (windowWidth > scope.state.basicWidth ? scope.state.basicWidth : windowWidth);
                scope.style.height = scope.state.percentHeight ? windowHeight / 100 * scope.state.percentHeight : (windowHeight > scope.state.basicHeight ? scope.state.basicHeight : windowHeight);
                scope.style.left = (windowWidth / 2) - (scope.style.width / 2);
                scope.style.top = (windowHeight / 2) - (scope.style.height / 2);
            };
            scope.addStyles = function(elem, styles){
                for(var key in styles){
                    elem.style[key] = styles[key];
                }
                elem.style.width = styles.width + 'px';
                elem.style.height = styles.height + 'px';
                elem.style.top = styles.top + 'px';
                elem.style.left = styles.left + 'px';
            };
            scope.getAutoHeight = function(){
                if(height === 'auto' && window.style.height === 'auto'){
                    scope.state.basicHeight = window.clientHeight;
                    scope.style.height = scope.state.basicHeight;
                }
            };

            scope.style['box-sizing'] = 'border-box';
            scope.style['margin-left'] = 0;
            scope.style['margin-top'] = 0;
            scope.style['max-width'] = 'none';
            scope.style['max-height'] = 'none';
            scope.style['user-select'] = 'none';


            scope.state = {
                move: false,
                resize: false,
                percentWidth: width.indexOf('%') !== -1 ? (parseInt(width.replace('%', '')) || 0) : 0,
                percentHeight: height.indexOf('%') !== -1 ? (parseInt(height.replace('%', '')) || 0) : 0,
                basicWidth: ((width.indexOf('%') !== -1) ? WIDTH : (parseInt(width) || WIDTH)),
                basicHeight: ((height.indexOf('%') !== -1) ? HEIGHT : (parseInt(height) || HEIGHT))
            };

            scope.calculateWindowParams($window.innerWidth, $window.innerHeight);
            scope.addStyles(window, scope.style);


            angular.element($window).bind('resize', function(){
                scope.calculateWindowParams($window.innerWidth, $window.innerHeight);
                scope.addStyles(window, scope.style);
            });
            angular.element(overlay.getElementsByClassName('pop-up_window_title')[0]).bind('mousedown', function(e){
                scope.state.move = true;
                scope.state.x = e.clientX;
                scope.state.y = e.clientY;
            });
            angular.element(overlay.getElementsByClassName('pop-up_window_resize_btn')[0]).bind('mousedown', function(e){
                scope.state.resize = true;
                scope.state.x = e.clientX;
                scope.state.y = e.clientY;
            });
            angular.element(overlay).bind('mousemove', function(e){
                if(scope.state.resize || scope.state.move){
                    var X = e.clientX;
                    var Y = e.clientY;

                    scope.getAutoHeight();

                    if(scope.state.resize){
                        var newWidth = scope.style.width + (X - scope.state.x);
                        var newHeight = scope.style.height + (Y - scope.state.y);

                        if(newWidth >= scope.state.basicWidth){
                            scope.style.width = newWidth;
                            scope.state.x = X;
                        }
                        if(newHeight >= scope.state.basicHeight){
                            scope.style.height = newHeight;
                            scope.state.y = Y;
                        }
                        scope.addStyles(window, scope.style);
                    }else if(scope.state.move){
                        var newLeft = scope.style.left + (X - scope.state.x);
                        var newTop = scope.style.top + (Y - scope.state.y);

                        if(newLeft < 0){
                            newLeft = 0;
                            X = scope.state.x - scope.style.left;

                        }else if(newLeft + scope.style.width > $window.innerWidth){
                            newLeft = $window.innerWidth - scope.style.width;
                            X = scope.state.x + (newLeft - scope.style.left);
                        }
                        if(newTop < 0){
                            newTop = 0;
                            Y = scope.state.y - scope.style.top;
                        }else if(newTop + scope.style.height > $window.innerHeight){
                            newTop = $window.innerHeight - scope.style.height;
                            Y = scope.state.y + (newTop - scope.style.top);
                        }
                        scope.style.left = newLeft;
                        scope.style.top = newTop;
                        scope.state.x = X;
                        scope.state.y = Y;
                        scope.addStyles(window, scope.style);
                    }
                }

            });
            angular.element(document).bind('mouseup', function(e){
                scope.state.move = false;
                scope.state.resize = false;
                scope.state.x = 0;
                scope.state.y = 0;
            });
            if(height === 'auto'){
                scope.$watch(function($scope) { return scope.open },
                    function(newValue, oldValue) {
                        if(newValue !== oldValue && newValue){
                            window.style.height = 'auto';
                        }
                    }
                );
            }
        }
    };
}]);

directives.directive('wizardTab',['$compile', '$injector', function($compile) {
    return {
        restrict: "A",
        scope: {
            wizardTab:'='
        },
        terminal: true,
        link: function(scope, element, attr){
            scope.click = function(TAB_ID){
                if(!scope.wizardTab.disabled){
                    scope.wizardTab.choose = TAB_ID;
                    if(scope.wizardTab.change){
                        scope.wizardTab.change(TAB_ID)
                    }
                }
            };

            element.prepend(
                '<div>' +
                '<ul class="tab_list {{wizardTab.disabled ? \'tab_list_disabled\' : \'\'}}">' +
                '<li ng-repeat="(TAB_ID, TAB) in wizardTab.tabs track by $index" ' +
                'class="{{wizardTab.choose == TAB_ID ? \'active\' : \'\'}}" ' +
                'ng-click="click(TAB_ID)" ' +
                'style="width:'+ (100 / Object.keys(scope.wizardTab.tabs).length) +'%">{{TAB.title}}' +
                '</li>' +
                '</ul>' +
                '</div>'
            );
            $compile(element.find('div'))(scope);
        }
    };
}]);
directives.directive('specialSelect',['$compile', '$document', '$parse', function($compile, $document, $parse){
    return {
        restrict: "A",
        //scope:true,
        scope:{
            selectOption:'=',
            ngModel: '=',
            chooseOption: '=',
            ifOption: '=',
            chooseOptionId: '=',
            insteadValue: '=',
            selectDisabled: '='
        },
        transclude: true,
        link: function(scope, element, attr){
            /*  scope.ngModel = $parse(attr.ngModel)(scope); scope.selectOption = $parse(attr.selectOption)(scope);*/
            scope.key = attr.insteadKey ? attr.insteadKey : 'key';
            scope.value = scope.insteadValue ? scope.insteadValue : ['value'];
            scope.emptyRow = attr.emptyRow === undefined ? false : (attr.emptyRow ? attr.emptyRow : 'Выберите вариант');
            scope.emptyValueText = attr.emptyValueText === undefined ? false : attr.emptyValueText;
            scope.searchText = attr.searchText === undefined ? 'Введите название' : attr.searchText;

            scope.search = {
                search:false,
                searchInput: ''
            };
            scope.select = {
                isOpen: false,
                scale: attr.scaleSelect !== undefined
            };
            scope.parentController = scope.$parent;

            scope.getOptionId = function(){
                for(var id in scope.selectOption){
                    if(scope.selectOption[id][scope.key] == scope.ngModel){
                        return id
                    }
                }
                return '';
            };

            if(scope.chooseOptionId === undefined){
                scope.chooseOptionId = scope.getOptionId();
            }
            if(scope.ngModel === undefined){
                scope.ngModel = scope.chooseOption[scope.chooseOptionId];
            }

            scope.changeValue = function(id){
                if(scope.emptyRow && id === ''){

                    scope.chooseOptionId = id;
                    scope.ngModel = '';
                }else{
                    scope.chooseOptionId = id;
                    scope.ngModel = scope.selectOption[id][scope.key];
                }

                scope.closeOptions();
                if(scope.chooseOption){
                    scope.chooseOption(id,  scope.selectOption[id]);
                }
            };
            scope.getRowValues = function(option){
                var row = '';
                if(option){
                    for(var key in scope.value){
                        var val = scope.value[key];
                        row = option[val] ? row + (row ? ' ' : '') + option[val] : row;
                    }
                }
                if(!row && scope.emptyRow){
                    row = scope.emptyRow;
                }
                if(!row && scope.emptyValueText){
                    row = scope.emptyValueText;
                }
                return row;
            };
            scope.toggleOptions = function(){
                if(scope.select.isOpen){
                    scope.closeOptions();
                }else{
                    scope.openOptions();
                    scope.search.search = true;
                }
            };
            scope.closeOptions = function(){
                scope.select.isOpen = false;
                scope.dropSearch();
            };
            scope.openOptions = function(){
                scope.select.isOpen = true;
            };
            scope.titleFocus = function(){
                scope.search.search = true;
            };
            scope.dropSearch = function(){
                scope.search.searchInput = '';
                scope.search.search = false;
            };
            scope.searchIf = function(option, searchInput){
                var row = '';
                for(var key in scope.value){
                    row = row + (row == '' ? '' : ' ') + option[scope.value[key]]
                }
                var result = (row.toUpperCase()).indexOf(searchInput.toUpperCase()) !== -1 || searchInput === '';

                return result;
            };

            element.append('<div></div>');
            var select = element.find('div');
            select.append(
                '<div  click-out-element="closeOptions">' +
                '<div class="select_imitation  {{select.scale ? \'rotate-select\' : \'\'}}" ng-if="!selectDisabled">' +
                '<div class="select_imitation_head">' +
                '<div class="select_imitation_title" ng-click="titleFocus()">' +
                '<div class="select_imitation_title_block" ng-click="toggleOptions()">' +
                '<div class="select_imitation_choose_option">' +
                '<div>' +
                //'<span ng-repeat="(valID, val) in value track by $index">{{selectOption[chooseOptionId][val]}} </span> ' +
                '{{getRowValues(selectOption[chooseOptionId])}}' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '<div class="select_imitation_toggle_arrow" ng-click="toggleOptions()"></div>' +
                '</div>' +
                '<div class="select_imitation_options_container" ng-if="select.isOpen || search.search">' +
                '<div class="select_imitation_search_input_container" ng-if="search.search">' +
                '<input type="text" class="select_imitation_search_input" ng-model="search.searchInput"  my-autofocus placeholder="{{searchText}}">' +
                '</div>' +
                '<div class="select_imitation_options" ng-if="select.isOpen || search.search">' +
                '<div ng-if="emptyRow">' +
                '<div class="select_imitation_option"  ng-click="changeValue(\'\')" >' +
                '{{emptyRow}}' +
                '</div>' +
                '</div>'+
                '<div ng-repeat="(optionID, option) in selectOption track by $index" ' +
                'ng-if="(option.if === undefined || option.if()) && (!search.search ||(search.search && searchIf(option,search.searchInput))) && (ifOption === undefined || ifOption(option, optionID))">' +
                '<div class="select_imitation_option"  ng-click="changeValue(optionID)">' +
                '<div>' +
                '{{getRowValues(option)}}' +
                //'<span ng-repeat="(valID, val) in value track by $index">{{option[val]}} </span>' +
                '</div>'+
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '<div class="select_imitation select_disabled"  ng-if="selectDisabled">' +
                '<div class="select_imitation_head">' +
                '<div class="select_imitation_title">' +
                '<div class="select_imitation_title_block">' +
                '<div ng-if="selectOption[chooseOptionId]">' +
                '<span ng-repeat="(valID, val) in value track by $index">{{selectOption[chooseOptionId][val]}} </span> ' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>'
            );

            $compile(select)(scope);

            scope.checkKinship = function(parent, possibleChild){ //Проверить, является ли элемент parent родителем элементу possibleChild
                var node = possibleChild.parentNode;
                while (node != null) {
                    if (node == parent) {
                        return true;
                    }
                    node = node.parentNode;
                }
                return false;
            };
            var windowClickForSelect = function windowClickForSelect(e){

                if(scope.select.isOpen && e.target !== element[0] &&  !scope.checkKinship(element[0], e.target)){
                    scope.$apply(function(){
                        scope.closeOptions();
                    });
                }
            };
            $document.bind('click', windowClickForSelect);
            scope.$on('$destroy',function(){
                $document.unbind('click', windowClickForSelect);
            });

            scope.$watch(function($scope) { return scope.ngModel },
                function(newValue, oldValue) {
                    if(newValue !== oldValue){
                        scope.chooseOptionID = scope.getOptionId();
                    }
                }
            );
        }
    };
}]);

directives.directive('myAutofocus',[function(){
    return {
        restrict: "A",
        link: function(scope, element, attr){
            element[0].focus()
        }
    };
}]);

directives.directive('clickOutElement',['$parse', '$document', function($parse, $document){
    return {
        restrict: "A",
        link: function(scope, element, attr){
            var click = $parse(attr.clickOutElement)(scope);
            var checkKinship = function(parent, possibleChild){ //Проверить, является ли элемент parent родителем элементу possibleChild
                var node = possibleChild.parentNode;
                while (node != null) {
                    if (node == parent) {
                        return true;
                    }
                    node = node.parentNode;
                }
                return false;
            };
            var windowClick = function windowClick(e){
                if(e.target !== element[0] && !checkKinship(element[0], e.target)){
                    scope.$apply(function(){
                        click();
                    });
                }
            };
            $document.bind('click', windowClick);
            scope.$on('$destroy',function(){
                $document.unbind('click', windowClick);
            });
        }
    };
}]);

directives.directive('validationError',['$parse', '$compile', function($parse, $compile){
    return {
        restrict: "A",
        scope: {
            validationError: '=',
            ngModel: '=',
            showError: '=',
        },
        link: function(scope, element, attr){

            scope.getError = function(){
                return scope.$parent.getErrorField(scope.ngModel, scope.validationError);
            };

            element.prepend('<div style="color: red; font-size: 10px;">{{showError ? getError() : \'\'}}</div>');
            $compile(element.find('div'))(scope);
        }
    };
}]);