<?php
/**
 * Created by PhpStorm.
 * User: max
 * Date: 29.03.2021
 * Time: 18:34
 */
namespace app\helpers;

use Yii;

class JsonResponse {

    public static function error($code, $errors) {
        Yii::$app->response->statusCode = $code;
        echo json_encode($errors);
        Yii::$app->end();
    }

    public static function success($data) {
        echo json_encode(['result' => $data]);
        Yii::$app->end();
    }

}