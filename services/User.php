<?php
/**
 * Created by PhpStorm.
 * User: max
 * Date: 22.03.2021
 * Time: 15:23
 */

namespace app\services;


use Yii;

class User extends SoapDto {

    /**
     * @var string
     * @soap
     */
    public $_1C_guid;

    /**
     * @var string
     * @soap
     */
    public $fio;

    public function __construct($generateRandomData = false) {
        if($generateRandomData) {
            $this->_1C_guid = Yii::$app->security->generateRandomString();
            $this->fio = 'Иванов Иван Иванович'.rand(0,100);
        }

    }
}