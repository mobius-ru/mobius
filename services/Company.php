<?php
/**
 * Created by PhpStorm.
 * User: max
 * Date: 22.03.2021
 * Time: 15:23
 */

namespace app\services;


class Company extends SoapDto {
    /**
     * @var string
     * @soap
     */
    public $name;

    /**
     * @var string
     * @soap
     */
    public $inn;

    /**
     * @var string
     * @soap
     */
    public $kpp;

    /**
     * @var string
     * @soap
     */
    public $ogrn;

    public function __construct($generateRandomData = false) {
        if($generateRandomData) {
            $this->name = 'ООО Компания'.rand(0,100);
            $this->inn = (string)rand(0000000000,9999999999);
            $this->kpp = (string)rand(000000000,999999999);
            $this->ogrn = (string)rand(000000000000,999999999999);
        }

    }
}