<?php
/**
 * Created by PhpStorm.
 * User: max
 * Date: 22.03.2021
 * Time: 15:24
 */

namespace app\services;


class Contract extends SoapDto {

    /**
     * @var string
     * @soap
     */
    public $number;

    /**
     * @var \DateTime
     * @soap
     */
    public $date_of_conclusion;

    /**
     * @var string
     * @soap
     */
    public $status;

    public function __construct($generateRandomData = false) {
        if($generateRandomData) {
            $this->number = 'МБ2021-'.rand(0,1000);
            $this->date_of_conclusion = date('Y-m-d H:i:s');
            $this->status = 1;
        }

    }
}