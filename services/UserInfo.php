<?php
/**
 * Created by PhpStorm.
 * User: max
 * Date: 22.03.2021
 * Time: 15:49
 */

namespace app\services;


class UserInfo extends SoapDto {
    /**
     * @var User
     * @soap
     */
    public $user;

    /**
     * @var Company
     * @soap
     */
    public $company;

    /**
     * @var Contract[]
     * @soap
     */
    public $contracts;

    /**
     * UserInfo constructor.
     * @param bool $generateRandomData
     */
    public function __construct($generateRandomData = false) {
        if($generateRandomData) {
            $this->user = new User(true);
            $this->company = new Company(true);
            $this->contracts[] =  new Contract(true);
        } else {
            $this->user = new User();
            $this->company = new Company();
            $this->contracts = [];
        }

    }
}