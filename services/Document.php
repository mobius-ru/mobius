<?php
/**
 * Created by PhpStorm.
 * User: max
 * Date: 29.03.2021
 * Time: 20:05
 */
namespace app\services;


class Document extends SoapDto {

    /**
     * @var int
     * @soap
     */
    public $type;

    public $path;

    public $name;

    /**
     * @var string
     * @soap
     */
    public $status;

    public function __construct($generateRandomData = false, $type = \app\models\Document::TYPE_CUSTOM) {
        if($generateRandomData) {
            $this->type = $type;
            $this->path = 'random str';
            $this->name = 'Имя документа '.rand(1,1000);
            $this->status = 1;
        }

    }
}