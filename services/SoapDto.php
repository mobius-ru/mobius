<?php
/**
 * Created by PhpStorm.
 * User: max
 * Date: 22.03.2021
 * Time: 15:59
 */

namespace app\services;


use yii\db\ActiveRecord;

class SoapDto {

    /**
     * @param SoapDto $_1C_object
     * @param ActiveRecord $AR_object
     */
    public static function mapFrom1C($_1C_object, &$AR_object){
        if($_1C_object) {
            $AR_object->setAttributes(is_array($_1C_object) ? $_1C_object : get_object_vars($_1C_object));
        }
    }

    /**
     * @param ActiveRecord $AR_object
     * @param SoapDto $_1C_object
     */
    public static function mapTo1C($AR_object, &$_1C_object) {
        if($AR_object) {
            foreach ($AR_object->attributes as $attributeName => $value) {
                $_1C_object->$attributeName = $value;
            }
        }
    }
}