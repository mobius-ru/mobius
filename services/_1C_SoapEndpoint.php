<?php
/**
 * Created by PhpStorm.
 * User: max
 * Date: 22.03.2021
 * Time: 15:22
 */
namespace app\services;

use yii\httpclient\Client;
use yii\httpclient\Exception;

class _1C_SoapEndpoint /*extends SoapClient*/ {

    private $_client;
    private $_login;
    private $_password;

    public function __construct() {
        $this->_client = new Client([
            'baseUrl' => \Yii::$app->params['1c_endpoint']['baseUrl'],
            'responseConfig' => [
                'format' => Client::FORMAT_JSON
            ]]);
        $this->_login = \Yii::$app->params['1c_endpoint']['login'];
        $this->_password = \Yii::$app->params['1c_endpoint']['password'];
    }

    public function getUserInfoByContractNumber($contractNumber) {
        $response = $this->_client
            ->get('Contracts/GetDataContract', ['number' => $contractNumber])
            ->addHeaders(['Authorization' => 'Basic ' . base64_encode($this->_login . ":". $this->_password)])
            ->send();
        if($response->isOk) {
            $respObj = json_decode( $response->getContent())[0];
            $result = new UserInfo();
            $result->company->name = $respObj->contractor->name;
            $result->company->inn = $respObj->contractor->inn;
            $result->company->kpp = $respObj->contractor->kpp;
            $result->company->ogrn = $respObj->contractor->ogrn;
            $result->user->fio = $respObj->contractor->name;
            $result->user->_1C_guid = $respObj->contractor->guid;
            $result->contracts = $respObj->contractor->contracts;
        } else {
            throw new Exception($response->getContent(),$response->getStatusCode());
        }
        return $result;
    }

    public function sendAcceptanceRequest() {
        $result = new Document(true, \app\models\Document::TYPE_ACCEPTANCE_REQUEST);
        return $result;
    }

}